// require data.js
var curChar = '';

document.addEventListener('DOMContentLoaded', function() {
  run();
});


document.addEventListener('keydown', function(event) {
  // On key down:
  // - get key pressed (and the char pressed)
  const keyCode = event.keyCode;
  const allowedKeycodes = getAllowedKeycodes();
  if (allowedKeycodes.includes(keyCode)) {
    // keycode to zhuyin char
    const zhPressed = keyCodeToZh(keyCode);
    if (curChar == zhPressed) {
      curChar = randZh();
      draw();
    } else {
      flashChar();
    }
  }
});


function getAllowedKeycodes() {
  let ret = [];
  Object.keys(ch).map((key, index) => {
    ret.push(ch[key][2]);
  });
  ret.push(32);
  return ret;
}


function flashChar() {
  const charDiv = document.getElementById('ch');
  charDiv.style.backgroundColor = 'red';
  const hint = document.getElementById('hint');
  hint.innerHTML = ch[curChar][1];
  setTimeout(function() {
    charDiv.style.backgroundColor = null;
  hint.innerHTML = '';
  }, 100);

}


function keyCodeToZh(keycode) {
  // e.g. 39 -> ㄅ
  for (key of Object.keys(ch)) {
    if (ch[key][2] == keycode) return key;
  }
  return false;
}


function draw() {
  var a = document.getElementById('ch');
  a.innerHTML = curChar;
}


function randZh() {
  // Return random zhuyin char, e.g. ㄅ
  // Note: Will not return char equal to the latest curChar
  const chars = Object.keys(ch);
  while (true) {
    const randInt = Math.floor(Math.random() * chars.length);
    let c = chars[randInt];
    if (c != curChar) {
      return c;
    }
  }
}


function run() {
  curChar = randZh();
  draw();
}
